// Custom select
document.querySelectorAll('.custom-select').forEach(function (selectWrapper) {
    const selectBtn = selectWrapper.querySelector('.custom-select__button');
    const selectList = selectWrapper.querySelector('.custom-select__list');
    const selectItems = selectList.querySelectorAll('.custom-select__item');
    const selectSelect = selectWrapper.querySelector('.custom-select__select');


    // Клик по кнопке. Открыть/Закрыть select
    selectBtn.addEventListener('click', function (e) {
        selectList.classList.toggle('_visible');
        this.classList.toggle('_active');
    });

    // Выбор элемента списка. Запомнить выбранное значение. Закрыть select
    selectItems.forEach(function (item) {
        item.addEventListener('click', function (e) {
            e.stopPropagation();
            selectBtn.innerHTML = this.innerHTML;
            selectBtn.focus();
            selectSelect.value = this.dataset.value;
            selectBtn.classList.remove('_active');
            selectList.classList.remove('_visible');
        });
    });

    // Клик снаружи select. Закрыть select
    document.addEventListener('click', function (e) {
        if (e.target !== selectBtn) {
            selectBtn.classList.remove('_active');
            selectList.classList.remove('_visible');
        }
    });

    // Нажатие на Tab или Escape. Закрыть select
    document.addEventListener('keydown', function (e) {
        if (e.key === 'Tab' || e.key === 'Escape') {
            selectBtn.classList.remove('_active');
            selectList.classList.remove('_visible');
        }
    });
});